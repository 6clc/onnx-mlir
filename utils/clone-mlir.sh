git clone https://github.com/llvm/llvm-project.git
# Check out a specific branch that is known to work with ONNX-MLIR.
cd llvm-project && git checkout 2c8a77ab21ff3a41829a5d67e0b838cc7a9f5f21 && cd ..
